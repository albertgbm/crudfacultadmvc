/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.albert.dao;

import com.albert.entities.EstudiantePrograma;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alhernan
 */
@Stateless
public class EstudianteProgramaFacade extends AbstractFacade<EstudiantePrograma> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EstudianteProgramaFacade() {
        super(EstudiantePrograma.class);
    }
    
}
