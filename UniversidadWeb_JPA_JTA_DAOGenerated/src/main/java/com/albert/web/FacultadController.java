/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.albert.web;

import com.albert.dao.AbstractFacade;
import com.albert.dao.FacultadFacade;
import com.albert.entities.Facultad;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author alhernan
 */
@WebServlet(name = "FacultadController", urlPatterns = {"/FacultadController"})
public class FacultadController extends HttpServlet {

    @EJB
    private FacultadFacade facultadFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FacultadController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FacultadController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String accion = request.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "create":
                   this.create(request, response);
                    break;
                case "read":
                    this.read(request, response);
                    break;
                case "update":
                    this.update(request, response);
                    break;
                case "delete":
                    //this.eliminarCliente(request, response);
                    break;
                default:
                    this.accionDefault(request, response);
                    break;
            }
        } else {
            this.accionDefault(request, response);
        }
        
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String accion = request.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "insert":
                   this.insert(request, response);
                    break;
                case "edit":
                    this.edit(request, response);
                    break;
                case "delete":
                    //this.eliminarCliente(request, response);
                    break;
                default:
                    this.accionDefault(request, response);
                    break;
            }
        } else {
            this.accionDefault(request, response);
        }
    }
    
    private void accionDefault(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        read(request, response);
    }
    
    private void read(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        List<Facultad> facultades = facultadFacade.findAll();
        System.out.println("facultades = " + facultades);
        
        HttpSession sesion = request.getSession();
        sesion.setAttribute("facultades", facultades);
        
        //request.getRequestDispatcher("clientes.jsp").forward(request, response);
        response.sendRedirect("facultad/facultad.jsp");
    }
    
    private void create(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        //redirigimos a la vista que contiene el formulario para registrar la facultad
        response.sendRedirect("facultad/create.jsp");
    }
    
    private void update(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
       //recuperamos el idFacultad
        int idFacultad = Integer.parseInt(request.getParameter("idFacultad"));
        Facultad f = new Facultad();
        f.setId(idFacultad);
        f = facultadFacade.find(f.getId());
        request.setAttribute("facultad", f);
        request.getRequestDispatcher("facultad/edit.jsp").forward(request, response);
    }
    
    private void edit(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
         //recuperamos los valores del formulario agregarCliente
        String nombre = request.getParameter("nombre");
        int idFacultad = Integer.parseInt(request.getParameter("idFacultad"));

        //Creamos el objeto de facultad (modelo)
        Facultad f = new Facultad();
        f.setId(idFacultad);
        f.setNombre(nombre);
        
        //Actualizamos el objeto en la base de datos
        facultadFacade.edit(f);

        //Redirigimos hacia el listado
        this.read(request, response);
    }
    

    protected void insert(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //recuperamos los valores del formulario agregarCliente
        String nombre = request.getParameter("nombre");

        //Creamos el objeto de facultad (modelo)
        Facultad f = new Facultad();
        f.setNombre(nombre);
        
        //Insertamos el nuevo objeto en la base de datos
        facultadFacade.create(f);

        //Redirigimos hacia el listado
        this.read(request, response);
        //this.create(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
