/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.albert.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alhernan
 */
@Entity
@Table(name = "estudiante_programa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstudiantePrograma.findAll", query = "SELECT e FROM EstudiantePrograma e"),
    @NamedQuery(name = "EstudiantePrograma.findByEstudianteId", query = "SELECT e FROM EstudiantePrograma e WHERE e.estudianteProgramaPK.estudianteId = :estudianteId"),
    @NamedQuery(name = "EstudiantePrograma.findByProgramaId", query = "SELECT e FROM EstudiantePrograma e WHERE e.estudianteProgramaPK.programaId = :programaId"),
    @NamedQuery(name = "EstudiantePrograma.findByFechaMatricula", query = "SELECT e FROM EstudiantePrograma e WHERE e.fechaMatricula = :fechaMatricula")})
public class EstudiantePrograma implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EstudianteProgramaPK estudianteProgramaPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "fecha_matricula")
    private String fechaMatricula;
    @JoinColumn(name = "estudiante_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Estudiante estudiante;
    @JoinColumn(name = "programa_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Programa programa;

    public EstudiantePrograma() {
    }

    public EstudiantePrograma(EstudianteProgramaPK estudianteProgramaPK) {
        this.estudianteProgramaPK = estudianteProgramaPK;
    }

    public EstudiantePrograma(EstudianteProgramaPK estudianteProgramaPK, String fechaMatricula) {
        this.estudianteProgramaPK = estudianteProgramaPK;
        this.fechaMatricula = fechaMatricula;
    }

    public EstudiantePrograma(int estudianteId, int programaId) {
        this.estudianteProgramaPK = new EstudianteProgramaPK(estudianteId, programaId);
    }

    public EstudianteProgramaPK getEstudianteProgramaPK() {
        return estudianteProgramaPK;
    }

    public void setEstudianteProgramaPK(EstudianteProgramaPK estudianteProgramaPK) {
        this.estudianteProgramaPK = estudianteProgramaPK;
    }

    public String getFechaMatricula() {
        return fechaMatricula;
    }

    public void setFechaMatricula(String fechaMatricula) {
        this.fechaMatricula = fechaMatricula;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estudianteProgramaPK != null ? estudianteProgramaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstudiantePrograma)) {
            return false;
        }
        EstudiantePrograma other = (EstudiantePrograma) object;
        if ((this.estudianteProgramaPK == null && other.estudianteProgramaPK != null) || (this.estudianteProgramaPK != null && !this.estudianteProgramaPK.equals(other.estudianteProgramaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.albert.entities.EstudiantePrograma[ estudianteProgramaPK=" + estudianteProgramaPK + " ]";
    }
    
}
