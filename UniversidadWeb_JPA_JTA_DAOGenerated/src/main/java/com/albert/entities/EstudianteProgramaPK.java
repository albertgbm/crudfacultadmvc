/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.albert.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alhernan
 */
@Embeddable
public class EstudianteProgramaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "estudiante_id")
    private int estudianteId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "programa_id")
    private int programaId;

    public EstudianteProgramaPK() {
    }

    public EstudianteProgramaPK(int estudianteId, int programaId) {
        this.estudianteId = estudianteId;
        this.programaId = programaId;
    }

    public int getEstudianteId() {
        return estudianteId;
    }

    public void setEstudianteId(int estudianteId) {
        this.estudianteId = estudianteId;
    }

    public int getProgramaId() {
        return programaId;
    }

    public void setProgramaId(int programaId) {
        this.programaId = programaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) estudianteId;
        hash += (int) programaId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstudianteProgramaPK)) {
            return false;
        }
        EstudianteProgramaPK other = (EstudianteProgramaPK) object;
        if (this.estudianteId != other.estudianteId) {
            return false;
        }
        if (this.programaId != other.programaId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.albert.entities.EstudianteProgramaPK[ estudianteId=" + estudianteId + ", programaId=" + programaId + " ]";
    }
    
}
