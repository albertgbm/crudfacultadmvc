<%-- 
    Document   : create
    Created on : 24/03/2022, 10:07:07 a. m.
    Author     : alhernan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Crear Facultad</h1>
        <form action="${pageContext.request.contextPath}/FacultadController?accion=insert" 
              method="POST" name="formCrearFacultad">
            <input type="text" name="nombre" placeholder="Digite el nombre de facultad"/>
            <input type="submit" value="Crear"/>
        </form>
        <br/>
        <a href="${pageContext.request.contextPath}/FacultadController?accion=read">Regresar</a>
    </body>
</html>
