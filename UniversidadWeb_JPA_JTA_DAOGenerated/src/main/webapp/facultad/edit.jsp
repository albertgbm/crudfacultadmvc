<%-- 
    Document   : edit
    Created on : 24/03/2022, 12:55:22 p. m.
    Author     : alhernan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Editar Facultad</h1>
        <form action="${pageContext.request.contextPath}/FacultadController?accion=edit&idFacultad=${facultad.id}" method="POST" 
              name="formActualizarFacultad">
            <input type="text" name="nombre" value="${facultad.nombre}" required placeholder="Digite el nombre de facultad"/>
            <input type="submit" value="Actualizar"/>
        </form>
        <br/>
        <a href="${pageContext.request.contextPath}/FacultadController?accion=read">Regresar</a>
    </body>
</html>
