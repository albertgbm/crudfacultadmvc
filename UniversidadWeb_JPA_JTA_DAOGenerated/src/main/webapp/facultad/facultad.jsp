<%-- 
    Document   : facultad
    Created on : 24/03/2022, 10:18:11 a. m.
    Author     : alhernan
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Facultades</h1>
        
        <a href="${pageContext.request.contextPath}/FacultadController?accion=create">Crear Facultad</a>
        <br/>
        <table border="1">
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Acción</th>
            </tr>
            <tbody>
                <!-- Iteramos cada elemento de la lista de facultades -->
            <c:forEach var="facultad" items="${facultades}" varStatus="status" >
                <tr>
                    <td>${status.count}</td>
                    <td>${facultad.nombre}</td>
                    <td>
                        <a href="${pageContext.request.contextPath}/FacultadController?accion=update&idFacultad=${facultad.id}">
                            Editar
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</body>
</html>
